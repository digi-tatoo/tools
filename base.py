# -*-coding:utf-8-*-
import math
import sys
import time
from zebra import Zebra

from cont import CALIBRATE_CMD, LABEL_L, LABEL_S


class ZebraPrint(Zebra):
    def __init__(self, queue,
                 label_height,
                 label_height_gap,
                 label_width,
                 file_loc,
                 direct_thermal=True,
                 sep='\t'):
        """
        queue - name of the printer queue,
        label_height - label height in dots,
        label_height_gap - label gap in dots,
        label_width - label width in dots
        """
        super(ZebraPrint, self).__init__(queue=queue)
        self.setup(direct_thermal=direct_thermal,
                   label_height=(label_height, label_height_gap),
                   label_width=label_width)
        self.sep = sep
        self.content = None

        self._load_tsv(file_loc)
        if self.content is None:
            input('无法正常读取tsv文件， 请按任意键退出并检查文件内容...')
            sys.exit(1)
        self._report()
        self.cmd_queue = []

    def _load_tsv(self, file_path):
        """
        Load tsv file
        Note: the tsv file has fixed format, please check the example.tsv
        """
        with open(file_path) as file:
            lines = file.readlines()[1:]
            self.content = []
            for line in lines:
                line = line.strip()
                self.content.append(line.split(self.sep))

    def _report(self):
        """
        Use report to check if the content is correct.
        """
        print('*'*20)
        print('请核对以下信息，确认无误')
        print(f'第一个SKU为{self.content[0][0]}, 数量是{self.content[0][-1]}')
        print(f'最后一个为{self.content[-1][0]}, 数量是{self.content[-1][-1]}')

        # calculate total labels
        total = 0
        for k in self.content:
            total += int(k[-1])
        print(f'总数是{total}个')
        print('*'*20)

        if input('如果不正确请按"1"退出,按任意其他键然后回车继续...') == "1":
            sys.exit(1)

    def calibrate_label(self):
        """
        Offer calibration option before printing.
        """
        calibrate = input('如果需要校准,请按"1",按任意其他键然后回车继续...')
        if calibrate == '1':
            self.output(CALIBRATE_CMD)

    @staticmethod
    def cmd_format(m_sku, fnsku, now_time):
        """
        Generate label cmd using actual content and template.
        If Merchant SKU is too long, separate it into two lines...
        """
        if len(m_sku) > 25:
            return LABEL_L.format(m_sku[0:25], m_sku[25:], fnsku=fnsku, today=now_time)
        else:
            return LABEL_S.format(m_sku, fnsku=fnsku, today=now_time)

    def print_queue(self):
        """
        Loop the tsv content loaded previously. And generate label cmds.
        """
        # Having time info on label for issue tracking.
        print_time = int(time.time())

        cmds = []
        for row in self.content:
            # in template it is 2 rows, so we have to divide the amount by 2 to calculate the correct row number
            for _ in range(math.ceil(int(row[-1])/2)):
                cmds.append(self.cmd_format(row[0], row[1], print_time))
        final_cmd = '\n'.join(cmds)

        print(final_cmd)
        input('按"1"退出， 按任意键开始打印...')
        self.output(final_cmd)
