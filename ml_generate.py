from cont import ML_TEMPLATE
import source

def generate_ml_label(sku, name, barcode, count):
    cmds=[]
    one_pair = ML_TEMPLATE.format(SKU=sku, Name=name, Barcode=barcode)
    for _ in range(round(count/2)):
        cmds.append(one_pair)
    cont = '\n'.join(cmds)

    with open(f'ml_generated/{sku}-{count}.txt', 'w') as f:
        f.write(cont)

if __name__ == "__main__":
    generate_ml_label(sku=source.sku,
                      name=source.name,
                      barcode=source.barcode,
                      count=source.count)
