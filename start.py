# -*- coding:utf-8 -*-
from base import ZebraPrint
import config
import os
import sys


if __name__ == '__main__':
    files = os.listdir(config.FILE_DIR)
    if len(files) != 1:
        input("input文件夹下面应该有且只有一个tsv文件...请核对后再重试...")
        sys.exit(1)

    FILE_LOC = config.FILE_DIR + os.sep + files[0]
    zp = ZebraPrint('ZDesigner GK888t (EPL)',
                    label_height=config.LABEL_HEIGHT,
                    label_height_gap=config.LABEL_HEIGHT_GAP,
                    label_width=config.LABEL_WIDTH,
                    file_loc=FILE_LOC)
    zp.calibrate_label()
    zp.print_queue()
