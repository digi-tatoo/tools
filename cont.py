# height in mm multiple 8
LABEL_HEIGHT = 240
LABEL_HEIGHT_GAP = 16
LABEL_WIDTH = 800

LABEL_L="""^XA
^FO40,24
^BCN,88,Y,N,N
^FD{fnsku}^FS
^FO40,160,^ADN,24,10^FD{0}^FS
^FO40,184,^ADN,24,10^FD{1}^FS
^FO40,216,^ADN,24,10^FD{today}^FS
^FO456,24
^BCN,88,Y,N,N
^FD{fnsku}^FS
^FO456,160,^ADN,24,10^FD{0}^FS
^FO456,184,^ADN,24,10^FD{1}^FS
^FO456,216,^ADN,24,10^FD{today}^FS
^XZ"""

LABEL_S="""^XA
^FO40,24
^BCN,88,Y,N,N
^FD{fnsku}^FS
^FO40,160,^ADN,24,10^FD{0}^FS
^FO40,208,^ADN,24,10^FD{today}^FS
^FO456,24
^BCN,88,Y,N,N
^FD{fnsku}^FS
^FO456,160,^ADN,24,10^FD{0}^FS
^FO456,208,^ADN,24,10^FD{today}^FS
^XZ"""

CALIBRATE_CMD = "~JC"

# This one is only for Mercado label file generation
ML_TEMPLATE = """^XA^CI28
^LH0,0
^FO22,170^A0N,18,18^FDSKU: {SKU}^FS
^FO22,170^A0N,18,18^FD^FS
^FB350,2,2
^FO22,150^A0N,18,18^FD^FS
^FO21,150^A0N,18,18^FD^FS
^FB400,2,2
^FO22,115^A0N,18,18^FD{Name}^FS
^FO65,18^BY2^BCN,54,N,N
^FD{Barcode}^FS
^FT150,98^A0N,22,22^FH\^FD{Barcode}^FS
^FT149,98^A0N,22,22^FH\^FD{Barcode}^FS
^PQ1,0,1,Y
^CI28
^LH0,0
^FO432,170^A0N,18,18^FDSKU: {SKU}^FS
^FO432,170^A0N,18,18^FD^FS
^FB350,2,2
^FO432,150^A0N,18,18^FD^FS
^FO431,150^A0N,18,18^FD^FS
^FB400,2,2
^FO432,115^A0N,18,18^FD{Name}^FS
^FO475,18^BY2^BCN,54,N,N
^FD{Barcode}^FS
^FT560,98^A0N,22,22^FH\^FD{Barcode}^FS
^FT559,98^A0N,22,22^FH\^FD{Barcode}^FS
^PQ1,0,1,Y^XZ"""
